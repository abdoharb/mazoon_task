<?php

namespace App\Http\Controllers;

use App\Order;
use Validator;
use Illuminate\Http\Request;

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = [
            'product_name' => 'required|string',
            'price' => 'required|numeric',
            'quantity' => 'required|numeric',
            'user_name' => 'required|string',
            'user_phone' => 'required',
            'user_address' => 'required|string'
        ];

        $inputs = [
            'product_name' => 'Product name',
            'price' => 'Product price',
            'quantity' => 'Quantity',
            'user_name' => 'User name',
            'user_phone' => 'Phone',
            'user_address' => 'Address'
        ];


        $validator = Validator::make($request->all(), $validation, [], $inputs);

        if ($validator->fails()) {
            return back()
                    ->withErrors($validator)
                    ->withInput();
        }

        // Calc Total
        $total = $request->price * $request->quantity;
        $request['total'] = $total;

        // Order number generate
        $request['order_number'] = $this->order_number_generate();

        $order = Order::create($request->except('_token'));

        return redirect()->route('orders.show', $order->id)->with('status', 'Success order');
    }

    // Order number generate
    protected function order_number_generate(){
        $last = Order::orderBy('id', 'desc');

        $code = $last->count() > 0 ? $last->first()->id + 10000 : 10000;

        return $code;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Order::findOrFail($id);

        return view('show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Order Actions
     */

    public function accept($id){
        $order = Order::findOrFail($id);

        $order->status = 1;
        $order->save();

        return back()->with('status', 'Success');
    }

    public function refuse($id){
        $order = Order::findOrFail($id);

        $order->status = 2;
        $order->save();

        return back()->with('status', 'Success');
    }
}
