@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Make Orders</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('orders.store') }}" class="form form-horizontal row">
                        @csrf
                        <!-- Product info -->
                        <div class="form-group col-md-12">
                            <h4 class="">Product information</h4>
                            <hr/>
                        </div>
                        <div class="form-group col-md-6 col-xs-12">
                            <label for="product_name">Product Name:</label>
                            <input type="text" name="product_name" value="{{ old('product_name') }}" class="form-control" id="product_name" />
                        </div>
                        <div class="form-group col-md-6 col-xs-12">
                            <label for="price">Product item Price:</label>
                            <input type="text" name="price" value="{{ old('price') }}" class="form-control" id="price" />
                        </div>
                        <div class="form-group col-md-6 col-xs-12">
                            <label for="quantity">Quantity:</label>
                            <input type="number" name="quantity" value="{{ old('quantity') }}" class="form-control" id="quantity" />
                        </div>

                        <!-- User info -->
                        <div class="form-group col-md-12 col-xs-12">
                            <h4 class="mt-3">User information</h4>
                            <hr/>
                        </div>
                        <div class="form-group col-md-6 col-xs-12">
                            <label for="user_name">Full name:</label>
                            <input type="text" name="user_name" value="{{ old('user_name') }}" class="form-control" id="user_name" />
                        </div>
                        <div class="form-group col-md-6 col-xs-12">
                            <label for="user_phone">Phone:</label>
                            <input type="text" name="user_phone" value="{{ old('user_phone') }}" class="form-control" id="user_phone" />
                        </div>
                        <div class="form-group col-md-6 col-xs-12">
                            <label for="user_address">Address:</label>
                            <input type="text" name="user_address" value="{{ old('user_address') }}" class="form-control" id="user_address" />
                        </div>

                        <div class="form-group col-md-12 mt-5">
                            <button type="submit" class="btn btn-primary btn-lg">Order Now !</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
