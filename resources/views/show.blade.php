@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Invoice number: #{{ $data->order_number }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <table class="table table-bordered table-hover">
                        <tbody>
                            <tr class="">
                                <td>Order number</td>
                                <td>{{ $data->order_number }}</td>
                                <td>Product</td>
                                <td>{{ $data->product_name }}</td>
                            </tr>
                            <tr>
                                <td>Price</td>
                                <td>{{ $data->price }}</td>
                                <td>Quantity</td>
                                <td>{{ $data->quantity }}</td>
                            </tr>
                            <tr>
                                <td>Total</td>
                                <td>{{ $data->total }}</td>
                                <td>User name</td>
                                <td>{{ $data->user_name }}</td>
                            </tr>
                            <tr>
                                <td>User phone</td>
                                <td>{{ $data->user_phone }}</td>
                                <td>User address</td>
                                <td>{{ $data->user_address }}</td>
                            </tr>
                            <tr>
                                <td>Status</td>
                                @if($data->status == 0)
                                    <td class="text-primary" colspan="3">pinding</td>
                                @elseif($data->status == 1)
                                    <td class="text-success" colspan="3">accepted</td>
                                @else
                                    <td class="text-danger" colspan="3">refuse</td>
                                @endif
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
