@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Orders</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr class="table-active">
                                <th>#</th>
                                <th>Order number</th>
                                <th>Product</th>
                                <th>Price</th>
                                <th>Quantity</th>
                                <th>Total</th>
                                <th>User name</th>
                                <th>User phone</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $x=0 @endphp
                            @foreach ($orders as $order)
                                @php $x++ @endphp
                                <tr>
                                    <td>{{ $x }}</td>
                                    <td>{{ $order->order_number }}</td>
                                    <td>{{ $order->product_name }}</td>
                                    <td>{{ $order->price }}</td>
                                    <td>{{ $order->quantity }}</td>
                                    <td>{{ $order->total }}</td>
                                    <td>{{ $order->user_name }}</td>
                                    <td>{{ $order->user_phone }}</td>
                                    @if($order->status == 0)
                                        <td class="text-primary">pinding</td>
                                    @elseif($order->status == 1)
                                        <td class="text-success">accepted</td>
                                    @else
                                        <td class="text-danger">refuse</td>
                                    @endif
                                    <td>
                                        <a class="text-success" href="{{ route('orders.accept', $order->id) }}">accept</a> /
                                        <a class="text-danger" href="{{ route('orders.refuse', $order->id) }}">refuse</a> /
                                        <a class="text-dark" href="{{ route('orders.show', $order->id) }}">show</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {!! $orders->links() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
